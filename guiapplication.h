#ifndef GUIAPPLICATION_H
#define GUIAPPLICATION_H


// local
#include "gamemodel.h"

// othello library
#include <engine.h>

// qt
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QSizeF>

// stl
#include <memory>


class GuiApplication : public QGuiApplication {
  Q_OBJECT
public:
  GuiApplication(int& argc, char** argv);
  ~GuiApplication() override = default;

private:
  othello::OthelloGameEngine m_game_engine;
  GameModel m_model;
  QQmlApplicationEngine m_app;
  bool prevPlayerHadMove = true;

private slots:
  void initNewHumanGame();
  void initNewHumanAndAIGame();
  void initNewAIGameWithMiniMaxAndMonkey();
  void endGameAndQuit();
  void endOfGameActions();
  void startNextTurn();
  void boardClicked(int);

signals:
  void enqueueNextTurn();
  void gameEnded();
  void displayFinalScores(int player_one_score, int player_two_score);

};   // END class GuiApplication

#endif   // GUIAPPLICATION_H
