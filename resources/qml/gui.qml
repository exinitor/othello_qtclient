import QtQuick 2.0
import QtQuick.Layouts 1.0
import QtQuick.Controls 1.4

ApplicationWindow {
  id: root

  signal initNewHumanGame()
  signal initNewGameVsMonkeyAI(string monkey_color)
  signal initNewMonkeyAIGame(string monkey_one_color, string monkey_two_color)
  signal initNewAIGameWithMiniMaxAndMonkey(string miniMax, string monkey_color)
  signal endGameAndQuit()
  signal boardClicked(int board_pos)

  signal displayFinalScores(int player_one_score, int player_two_score)

  onDisplayFinalScores: {
    final_score_window.player_one_final_score = player_one_score
    final_score_window.player_two_final_score = player_two_score
    final_score_window.visible = true
  }

  visible: true

  width: 800
  height: 600

  menuBar: MenuBar {
        Menu {
          title: "Game"
          MenuItem {
            text: "New Game: Human Endeavour"
            onTriggered: initNewHumanGame()
          }
          MenuItem {
              text: "New Game: one human and one AI"
              onTriggered: initNewGameVsMonkeyAI("orange monkey")
          }

          MenuItem {
              text: "New Game: miniMax AI and orange monkey AI"
              onTriggered: initNewAIGameWithMiniMaxAndMonkey("miniMax","orange monkey")
          }

          MenuItem {
            text: "Quit"
            onTriggered: endGameAndQuit()
          }
        }
    }


  GridLayout {
    anchors.fill: parent
    columns: 8
    rows: 8
    columnSpacing: 0.2
    rowSpacing: 0.2



    Repeater {
      model: VisualDataModel {
        model: gamemodel
        delegate: Rectangle {



            id: piece_rec
            property bool contains_mouse: false
            property string player_one_color: "black"
            property string player_two_color: "white"
            color: "green"

            Layout.fillHeight: true
            Layout.fillWidth: true
            Rectangle{
                anchors.centerIn: parent
                width: Math.min(parent.width, parent.height)*0.7
                height: width
                radius: width * 0.5

                color: {
                            if (occupied) {
                              if(playernr == 0) return player_one_color
                              else if(playernr == 1) return player_two_color
                              else return "black"
                            }
                            else if(contains_mouse) {
                              if(gamemodel.currentPlayer === 0) return player_one_color
                              else if(gamemodel.currentPlayer  === 1) return player_two_color
                              else return "black"
                            }
                            else return "green"
                          }
            }




            border.color: "black"
            border.width: 1
            MouseArea {
              anchors.fill: parent
              onClicked: {
                console.debug("Piece nr: " + piecenr)
                boardClicked(piecenr)
              }

              hoverEnabled: true
              onContainsMouseChanged: piece_rec.contains_mouse = containsMouse

            }
        }
      }
    }
  }

  Rectangle {
    id: final_score_window
    visible: false

    property int player_one_final_score: 0
    property int player_two_final_score: 0

    anchors.fill: parent
    anchors.margins: 50

    color: "teal"
    opacity: 0.8

    ColumnLayout {
      anchors.fill: parent
      anchors.margins: 20

      Text{ Layout.preferredWidth: parent.width; height: 20;
            text: "Game Over!" }
      Item{ height: 20}
      Text{ Layout.preferredWidth: parent.width; height: 20;
            text: "Player one: " + final_score_window.player_one_final_score}
      Text{ Layout.preferredWidth: parent.width; height: 20;
            text: "Player two: " + final_score_window.player_two_final_score}
      Item{ Layout.fillHeight: true}
      Button{ text: "Ok"; onClicked: final_score_window.visible = false }
    }

  }
}
