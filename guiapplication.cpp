#include "guiapplication.h"

// othello game library
#include <orangemonkey_ai.h>


// qt
#include <QQmlContext>
#include <QQuickItem>
#include <QQuickWindow>
#include <QTimer>
#include <QDebug>
#include <QThread>

// stl
#include <chrono>
using namespace std::chrono_literals;


GuiApplication::GuiApplication(int& argc, char** argv)
  : QGuiApplication(argc, argv),
    m_game_engine{}, m_model{m_game_engine}, m_app{}
{

  m_app.rootContext()->setContextProperty("gamemodel", &m_model);

  m_app.load(QUrl("qrc:/qml/gui.qml"));

  auto* root_window = qobject_cast<QQuickWindow*>(m_app.rootObjects().first());
  if (root_window) {

    connect(root_window, SIGNAL(endGameAndQuit()), this,
            SLOT(endGameAndQuit()));

    connect(root_window, SIGNAL(boardClicked(int)), this,
            SLOT(boardClicked(int)));

    connect(root_window, SIGNAL(initNewHumanGame()), this,
            SLOT(initNewHumanGame()));

    connect(root_window, SIGNAL(initNewGameVsMonkeyAI(QString)), this,
            SLOT(initNewHumanAndAIGame()));

    connect(root_window, SIGNAL(initNewAIGameWithMiniMaxAndMonkey(QString,QString)), this,
            SLOT(initNewAIGameWithMiniMaxAndMonkey()));

    connect(this, &GuiApplication::gameEnded, this,
            &GuiApplication::endOfGameActions);
    connect(this, &GuiApplication::enqueueNextTurn,
            this, &GuiApplication::startNextTurn);


    connect(this, SIGNAL(displayFinalScores(int,int)), root_window,
            SIGNAL(displayFinalScores(int,int)));
    m_game_engine.initNewGame();
    m_model.update();
  }
}

void GuiApplication::startNextTurn()
{

    if(m_game_engine.legalMovesCheck())
    {
        if(!prevPlayerHadMove)
        {
            m_model.update();
            emit gameEnded();
            return;
        }

        prevPlayerHadMove = false;
        m_game_engine.switchCurrentPlayerId();
        m_model.update();
        emit enqueueNextTurn();
        return;
    }
    prevPlayerHadMove = true;

    if(m_game_engine.currentPlayerType() == othello::PlayerType::Human)
        return;
    m_game_engine.think(2s);
    m_model.update();
    emit enqueueNextTurn();
}

void GuiApplication::boardClicked(int pos)
{
    auto success = m_game_engine.performMoveForCurrentHuman(othello::BitPos{pos});
    if (!success)
        return;
  m_model.update();
  startNextTurn();


}

void GuiApplication::initNewHumanGame()
{

    m_game_engine.initNewGame();
    m_model.update();
}

void GuiApplication::initNewHumanAndAIGame()
{
    m_game_engine.initNewGameWithOneAI();
    m_model.update();
}

void GuiApplication::initNewAIGameWithMiniMaxAndMonkey()
{
    m_game_engine.initNewGameWithTwoAI();
    m_model.update();
}



void GuiApplication::endGameAndQuit() { QGuiApplication::quit(); }


void GuiApplication::endOfGameActions()
{
    emit displayFinalScores(m_game_engine.board()[0].count(),m_game_engine.board()[1].count());

}
